# TS Automation

### Pre-Requisites :eyes: 
1. Ruby    
2. Bundler gem   
3. __For WEB__    
  a. Browsers (Chrome, Firefox, IE, Opera etc etc)     
  b. Respective WebDrivers for above mentioned browsers (placed in PATH variable)   
4. __For Android__  
  a. Appium
  b. adb
  c. Android Emulator (preferable 6.0)   
    
   
### Setup :eyes:
1. git clone <this repo>  (This will download this repo for you)
2. cd ts  (move into base directory)   
3. bundle install (This will install all dependent gems)   


### Execution :eyes:

##### Android All tests
`rake android BROWSER=android`

##### Web tests
`rake web BROWSER=chrome`  
`rake web BROWSER=firefox`  
`rake web BROWSER=opera`  
etc

##### Unit tests
`rake unit_tests`



### Reports :eyes:
Beautiful screenshot embedded reports are created in reports folder.   
Currently sample reports are present in reports folder for every execution(i.e Android, Web and Unit tests)  
These sample reports are in html format and can be viewed in a browser.   


### Author
__Kaushal Rupani__ :sunglasses:

  

  