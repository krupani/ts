require_relative './maths'

describe 'Maths' do

  describe "Addition" do

    it "should return valid result for positive numbers" do
      expect(Maths.new.add(7,3)).to eq 10
    end

    it "should return valid result for negative numbers" do
      expect(Maths.new.add(-7,-6)).to eq -13
    end

    it "should return valid result for mixed numbers" do
      expect(Maths.new.add(-7,16)).to eq 9
    end

    it "should return zero for alphabets" do
      expect(Maths.new.add("p","k")).to eq 0
    end

    it "should return valid integer result for float numbers" do
      expect(Maths.new.add(3.7,7.3)).to eq 10
    end

  end

  describe "Subtraction" do

    it "should return valid result for positive numbers" do
      expect(Maths.new.subtract(17,7)).to eq 10
    end

    it "should return valid result for negative numbers" do
      expect(Maths.new.subtract(-7,-6)).to eq -1
    end

    it "should return valid result for mixed numbers" do
      expect(Maths.new.subtract(7,-3)).to eq 10
    end

    it "should return zero for alphabets" do
      expect(Maths.new.subtract("p","k")).to eq 0
    end

    it "should return valid integer result for float numbers" do
      expect(Maths.new.subtract(3.7,7.3)).to eq -4
    end

  end

  describe "Multiplication" do

    it "should return valid result for positive numbers" do
      expect(Maths.new.multiply(3,7)).to eq 21
    end

    it "should return valid result for negative numbers" do
      expect(Maths.new.multiply(-7,-6)).to eq 42
    end

    it "should return valid result for mixed numbers" do
      expect(Maths.new.multiply(7,-3)).to eq -21
    end

    it "should return zero for alphabets" do
      expect(Maths.new.multiply("p","k")).to eq 0
    end

    it "should return valid integer result for float numbers" do
      expect(Maths.new.multiply(3.7,7.3)).to eq 21
    end

  end

  describe "Division" do

    it "should return valid result for positive numbers" do
      expect(Maths.new.divide(77,11)).to eq 7
    end

    it "should return valid result for negative numbers" do
      expect(Maths.new.divide(-16,-4)).to eq 4
    end

    it "should return valid result for mixed numbers" do
      expect(Maths.new.divide(36,-3)).to eq -12
    end

    it "should return valid integer result for float numbers" do
      expect(Maths.new.divide(95.7,5.3)).to eq 19
    end

    it "should return exception for denominator 0" do
      expect(Maths.new.divide(10,0)).to be_an_instance_of ZeroDivisionError
    end

    it "should return exception for alphabets" do
      expect(Maths.new.divide("p","k")).to be_an_instance_of ZeroDivisionError
    end

  end

end

