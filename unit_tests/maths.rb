class Maths

  def add(x,y)
    x.to_i+y.to_i
  end

  def subtract(x,y)
    x.to_i-y.to_i
  end

  def multiply(x,y)
    x.to_i*y.to_i
  end

  def divide(x,y)
    if y.to_i.nonzero?
      x.to_i/y.to_i
    else
      ZeroDivisionError.new
    end
  end

end