
Given(/^I enter valid login credentials$/) do
  login = LoginScreen.new(@driver)
  login.login_email_field.send_keys(@test_data["EMAIL"])
  login.login_password_field.send_keys(@test_data["PASSWORD"])
end

Then(/^I should be on home screen$/) do
  home = HomeScreen.new(@driver)
  expect(home.home_envelope_button.displayed?).to be true
end

When(/^I login$/) do
  login = LoginScreen.new(@driver)
  login.login_button.click
end

Given(/^I enter empty username$/) do
  login = LoginScreen.new(@driver)
  login.login_password_field.send_keys(@test_data["PASSWORD"])
  login.login_email_field.send_keys("")
end

Then(/^I verify empty username error message$/) do
  expect(text("This field is required").displayed?).to be true
end

Given(/^I login with valid credentials$/) do
  step("I enter valid login credentials")
  step("I login")
end

When(/^I logout$/) do
  home = HomeScreen.new(@driver)
  home.more_options_icon.click
  home.logout_button.click
  home.logout_confirm_yes.click
end

Then(/^I should be on the login screen$/) do
  login = LoginScreen.new(@driver)
  expect(login.login_email_field.displayed?).to be true
end