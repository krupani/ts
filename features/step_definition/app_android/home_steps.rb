When(/^I verify menu icon is( not)? visible$/) do |display_flag|
  home = HomeScreen.new(@driver)
  if display_flag
    expect(home.menu_icon.size).to eq 0
  else
    expect(home.menu_icon.first.displayed?).to be true
  end
end

And(/^I click on menu icon$/) do
  home = HomeScreen.new(@driver)
  home.menu_icon.first.click
end

Then(/^I verify (current|selected) date is highlighted$/) do |which|
  home = HomeScreen.new(@driver, @apppium)
  if which == "current"
    day = Time.now.day
  elsif which == "selected"
    day = @day
  end
  expect(home.highlighted_date.first.text).to eq day.to_s
end

Then(/^I verify (\d) date is highlighted$/) do |count|
  home = HomeScreen.new(@driver)
  expect(home.highlighted_date.size).to eq count.to_i
end

When(/^I select another date$/) do
  home = HomeScreen.new(@driver,@appium)
  Time.now.day==10 ? @day=15 : @day=10
  home.get_date_element(@day).click
  sleep 1
end

When(/^I click on the round button$/) do
  home = HomeScreen.new(@driver)
  home.home_envelope_button.click
end

Then(/^I verify popup from bottom of screen$/) do
  home = HomeScreen.new(@driver)
  expect(home.home_bottom_popup.text).to eq "Automate Me"
end

# Swipe method can be easily written in a utility file
# Currently writing here for shortage of time
When(/^I swipe (left|right) (\d+) time$/) do |direction,iteration|
  y_start = @height/2
  y_end = @height/2
  if direction == "left"
    x_start = @width-1
    x_end = 1
  elsif direction == "right"
    x_start = 150
    x_end = @width-1
  end

  iteration.to_i.times do
    Appium::TouchAction.new.swipe(:start_x => x_start, :start_y => y_start, :end_x => x_end, :end_y => y_end, :duration => 1000).perform
    sleep 1
  end
end

Then(/^I verify (previous|current|next) month is displayed$/) do |which|
  home = HomeScreen.new(@driver)
  case which
    when "previous"
      date = (Date.today.to_datetime << 1).strftime("%d %B %Y")
    when "next"
      date = (Date.today.to_datetime >> 1).strftime("%d %B %Y")
    else
      date = Date.today.strftime("%d %B %Y")
  end
  expect(home.get_date_element_by_full_date(date).displayed?).to be true
end
