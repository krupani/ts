And(/^I verify navigation menu is (open|closed)$/) do |toggle|
  menu = MenuScreen.new(@driver)
  if toggle=="open"
    expect(menu.menu_screen.first.displayed?).to be true
  elsif toggle=="closed"
    expect(menu.menu_screen.size).to eq 0
  end
end

When(/^I close the navigation menu$/) do
  home = HomeScreen.new(@driver)
  home.home_envelope_button.click
end

Then(/^I verify all menu items are visible$/) do
  menu = MenuScreen.new(@driver)
  expected_items = @test_data['MENU_ITEMS'].split(",")
  menu.menu_items.each_with_index do |item,i|
    expect(item.text).to eq expected_items[i]
  end
end

And(/^I verify logo is visible$/) do
  menu = MenuScreen.new(@driver)
  expect(menu.app_logo.displayed?).to be true
end

And(/^I verify user information is visible$/) do
  menu = MenuScreen.new(@driver,@appium,@test_data)
  expect(menu.app_user_name.displayed?).to be true
  expect(menu.app_user_name.text).to eq @test_data['NAME']
end