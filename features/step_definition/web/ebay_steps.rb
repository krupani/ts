Given(/^I am ebay home page$/) do
  @driver.get(@base_url)
end

When(/^I search for "([^"]*)" category$/) do |category|
  ebay = EbayPage.new(@driver)
  ebay.all_categories_element.click
  ebay.category_by_name(category).click
end

And(/^I select Buy Now option$/) do
  ebay = EbayPage.new(@driver)
  ebay.buy_it_now_button_element.click
end

Then(/^I add top (\d+) items to cart$/) do |count|
  ebay = EbayPage.new(@driver)
  product_urls = []
  ebay.products_elements.each_with_index do |product,i|
    product_urls << product.attribute('href')
    break if i==count.to_i-1
  end

  product_urls.each do |url|
    @driver.get(url)
    ebay.add_to_cart_button_element.click
  end
end

Then(/^I should be on the (cart|guest_checkout) page$/) do |page|
  ebay = EbayPage.new(@driver)
  case page
    when "cart"
      expect(ebay.header_element.text).to eq @test_data['PAGE']['CART']
    when "guest_checkout"
      expect(ebay.header_element.text).to eq @test_data['PAGE']['GUEST_CHECKOUT']
  end
end

And(/^I should see (\d+) items added to cart$/) do |count|
  ebay = EbayPage.new(@driver)
  expect(ebay.cart_items_elements.size).to eq count.to_i
  expect(ebay.cart_item_count_element.text).to eq count.to_s
end

When(/^I proceed to checkout$/) do
  ebay = EbayPage.new(@driver)
  ebay.proceed_to_checkout_element.click
end

And(/^I select checkout as guest$/) do
  ebay = EbayPage.new(@driver)
  ebay.checkout_as_guest_element.click
end

When(/^I submit all guest information$/) do
  ebay = EbayPage.new(@driver)
  ebay.guest_first_name_element.send_keys(Faker::Name.first_name)
  ebay.guest_last_name_element.send_keys(Faker::Name.last_name)
  ebay.guest_address_element.send_keys(Faker::Address.street_name)
  ebay.guest_city_element.send_keys(@test_data['GUEST']['CITY'])
  ebay.guest_state_element.send_keys(@test_data['GUEST']['STATE'])
  ebay.guest_zip_element.send_keys(@test_data['GUEST']['ZIP'])
  email = Faker::Internet.free_email
  ebay.guest_email_element.send_keys(email)
  ebay.guest_email_confirm_element.send_keys(email)
  ebay.guest_phone_number_element.send_keys(@test_data['GUEST']['PHONE'])
  ebay.guest_info_done_button_element.click
  sleep 5
end
