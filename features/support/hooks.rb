class AppiumWorld
end

Before do |scenario|
  # Initialize test data file
  @test_data = YAML.load_file(File.dirname(__FILE__) + "/../../data/test_data.yml")

  ENV['BROWSER']="chrome" if ENV['BROWSER'].nil?
  case ENV['BROWSER'].downcase
    when "android"
      launch_appium_driver
    else
      @driver = TestNow.init     # <-- This is my gem which takes care of browser configurations
      @base_url = "https://ebay.com"
  end
end

After do |scenario|
  if scenario.failed?
    begin
      encoded_img = @driver.screenshot_as(:base64)
      embed("#{encoded_img}", "image/png;base64")
    rescue
      p "*** Could not take failed scenario screenshot ***"
    end
  end
  quit_driver
end

AfterStep do
  begin
    encoded_img = @driver.screenshot_as(:base64)
    embed("#{encoded_img}", "image/png;base64")
  rescue
    p "*** Could Not take screenshot ***"
  end
end

def quit_driver
  @driver.quit
end



# Method to launch appium driver
def launch_appium_driver
  # Locate the apk file fron the data folder
  app_path = File.absolute_path("automateme.apk", "data")

  # List down the capabilities
  caps = {
      :platformName => "Android",
      :deviceName => "Android",
      # :uuid => "a4f3c47b",
      :app => app_path,
      :noReset => 'true',
      :newCommandTimeout => "90"
  }

  # Launch appium driver
  @driver = Appium::Driver.new(:caps => caps).start_driver
  @driver.manage.timeouts.implicit_wait = 10
  Appium.promote_appium_methods AppiumWorld
  @appium = AppiumWorld.new

  dimension = @driver.manage.window.size
  @height = dimension.height
  @width = dimension.width
end