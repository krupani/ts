require 'rspec'
require 'selenium-webdriver'
require 'cucumber'
require 'rake'
require 'appium_lib'
require 'testnow'
require 'faker'
require 'page-object'

include RSpec::Matchers
include TestNow