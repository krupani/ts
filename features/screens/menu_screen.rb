class MenuScreen

  def initialize(driver,appium=nil,test_data=nil)
    @driver = driver
    @appium = appium
    @test_data = test_data
  end

  def app_logo
    @driver.find_element(:id => "lib4.tigerspike.com.automateme:id/imageView")
  end

  def app_user_name
    @appium.text(@test_data['NAME'])
  end

  def menu_items
    @driver.find_elements(:id => "#lib4.tigerspike.com.automateme:id/design_menu_item_text")
  end

  def menu_screen
    @driver.find_elements(:id => "lib4.tigerspike.com.automateme:id/design_navigation_view")
  end

end