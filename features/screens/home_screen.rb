class HomeScreen

  def initialize(driver,appium=nil)
    @driver = driver
    @appium = appium
  end

  def home_envelope_button
    @driver.find_element(:id => "lib4.tigerspike.com.automateme:id/fab")
  end

  def menu_icon
    @driver.find_elements(:xpath => "//*[@content-desc='Open navigation drawer']")
  end

  def more_options_icon
    @driver.find_element(:xpath => "//*[@content-desc='More options']")
  end

  def home_bottom_popup
    @driver.find_element(:id => "lib4.tigerspike.com.automateme:id/snackbar_text")
  end

  def highlighted_date
    @driver.find_elements(:xpath => "//*[@checked='true']")
  end

  def get_date_element(day)
    @driver.find_element(:xpath => "//*[@text='#{day.to_s}']")
    # @driver.find_element(:xpath => "//android.view.View[#{day.to_s}]")
  end

  def get_date_element_by_full_date(date)
    @driver.find_element(:xpath => "//*[@content-desc='#{date}']")
  end

  def logout_button
    @driver.find_element(:id => "lib4.tigerspike.com.automateme:id/title")
  end

  def logout_confirm_yes
    @driver.find_element(:id => "android:id/button1")
  end

end