class LoginScreen

  def initialize(driver)
    @driver = driver
  end

  def login_email_field
    @driver.find_element(:id => "lib4.tigerspike.com.automateme:id/email")
  end

  def login_password_field
    @driver.find_element(:id => "lib4.tigerspike.com.automateme:id/password")
  end

  def login_button
    @driver.find_element(:id => "lib4.tigerspike.com.automateme:id/email_sign_in_button")
  end

  def login_empty_user_error
    text("This field is required")
  end

end