Feature: As an app user, I want to verify I can login to my application

  Scenario: Verify successful login
    Given I enter valid login credentials
    When I login
    Then I should be on home screen

  Scenario: Verify successful logout
    Given I login with valid credentials
    Then I should be on home screen
    When I logout
    Then I should be on the login screen



