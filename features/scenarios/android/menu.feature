Feature: As an app user, I verify I can open and see all menu items appropriately

  Background:
    Given I login with valid credentials

  Scenario: Verify menu toggle works
    When I verify menu icon is visible
    And I click on menu icon
    Then I verify menu icon is not visible
    And I verify navigation menu is open
    When I close the navigation menu
    Then I verify menu icon is visible
    And I verify navigation menu is closed

  Scenario: Verify all items are displayed in menu
    When I click on menu icon
    Then I verify all menu items are visible
    And I verify logo is visible
    And I verify user information is visible
