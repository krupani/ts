Feature: As an app user, I verify home page and home functionality works fine

  Background:
    Given I login with valid credentials

  Scenario: Verify current date is highlighted on home page
    Then I verify current date is highlighted
    And I verify 1 date is highlighted

  Scenario: Verify a popup on click of round button
    When I click on the round button
    Then I verify popup from bottom of screen

  Scenario: Verify swipe left/right displays previous/next month respectively
    When I swipe left 1 time
    Then I verify next month is displayed
    And I verify 0 date is highlighted
    When I swipe right 2 time
    Then I verify previous month is displayed
    And I verify 0 date is highlighted
    When I swipe left 1 time
    Then I verify current month is displayed
    And I verify 1 date is highlighted