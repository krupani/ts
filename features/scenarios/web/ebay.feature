Feature: As a ebay user, ensure I can buy 5 items

  Scenario: Verify user can buy 5 items
    Given I am ebay home page
    When I search for "Collectibles" category
    And I select Buy Now option
    Then I add top 5 items to cart
    And I should be on the cart page
    And I should see 5 items added to cart
    When I proceed to checkout
    And I select checkout as guest
    Then I should be on the guest_checkout page
    When I submit all guest information
