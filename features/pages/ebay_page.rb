class EbayPage
  include PageObject

  element :all_categories, {:id => "gh-shop-a"}
  element :buy_it_now_button, {:css => ".last_b"}
  elements :products, {:css => ".lvtitle>a"}
  element :add_to_cart_button, {:id => "isCartBtn_btn"}
  element :proceed_to_checkout, {:id => "ptcBtnRight"}
  element :checkout_as_guest, {:id => "gtChk"}
  element :header, {:css => "h1"}

  element :guest_first_name, {:id => "af-first-name"}
  element :guest_last_name, {:id => "af-last-name"}
  element :guest_address, {:id => "af-address1"}
  element :guest_city, {:id => "af-city"}
  element :guest_state, {:id => "af-state"}
  element :guest_zip, {:id => "af-zip"}
  element :guest_email, {:id => "af-email"}
  element :guest_email_confirm, {:id => "af-email-confirm"}
  element :guest_phone_number, {:css => ".ipt-phone"}
  element :guest_info_done_button, {:css => ".guest-address-btn"}

  elements :cart_items, {:css => "[id^='sellerBucket']"}
  element :cart_item_count, {:id => "gh-cart-n"}

  def initialize(driver)
    @driver = driver
    super
  end

  def category_by_name(category)
    @driver.find_element(:xpath => "//a[text()='#{category}']")
  end

end